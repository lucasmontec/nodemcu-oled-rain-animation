NODEMCU OLED Rain Animation
===========================

## Libraries

* [ThingPulse OLED SSD1306](https://github.com/ThingPulse/esp8266-oled-ssd1306) oled library to drive the display.
* Wire library as a dependency and to read other sensors over the I2C.
* ESP8266WiFi library to disable NODEMCU's wifi.

## Configuration

These are the screen sizes:
```c
#define SCREEN_W 128
#define SCREEN_H 64
```

This is the rain drop length (in pixels):
```c
#define DROP_LENGTH 8
```

This are the randomized drop speeds parameters (Both are pixels per second * 100):
```c
#define DROP_MIN_SPEED 90
#define DROP_MAX_SPEED 320
```

This are the parameters for drop spawning:
```c
//0 - 100
#define DROP_CHANCE 75
//Interval between try drop spawn in ms
#define DROP_INTERVAL 250
```
Every drop interval there is a chance of DROP_CHANCE percent of spawning a new rain drop.

## Notes about the code

### void createDrop(int X, float Y, float Speed)

This is an interesting method. The drop allocation is done dynamically inside the function.
This was causing a bug when I was allocating the new structure without a malloc. If you allocate
a drop inside a function using just a normal allocation like this: `drop d;` when the function
exits its scope, the variable d is destroyed thus leaving all of the neat pointers that you pointed
to it, pointing to garbage.

Also, remember to check if your allocation succeeded:

```c
if(d == NULL){
    //Serial.println("malloc failed");
    return;
}
```

### void removeDrop(drop* previous, drop* current)

This method was a good one, a nice bug. If you are getting exceptions and resets on NodeMCU and you
are using malloc() and free(), than this is something that you need to watchout for.

My linked list implementation was doing the allocation with the expected memory allocated to fit the
drop structure. That structure has am int, two floats and the pointer to the next drop. After setting
that pointer by inserting elements in my linked list, sometimes my Node was crashing with a poison check
exception. After much investigation I understood that the poison is a chunck of memory that is set after
an allocated memory block to check if when calling free() the user is trying to free more memory than was
initially allocated at that address. What was happening was that after setting the pointer to the next element
in the list, the pointer address was overriting the poison chunk, thus triggering the exception. 

The solution for this mess was freeing the next pointer by setting it to NULL before the call to free().
This is also why this code is guarded by so many esquizofrenic null checks.

```c
current->next = NULL;
```
