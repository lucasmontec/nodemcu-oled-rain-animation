#include <Wire.h>
#include "SSD1306Wire.h"
#include <ESP8266WiFi.h>

#include "extended_drawing.h"

#define SCREEN_W 128
#define SCREEN_H 64

//Drop length in pixels
#define DROP_LENGTH 8

//Both are pixels per second * 100
#define DROP_MIN_SPEED 90
#define DROP_MAX_SPEED 320

//0 - 100
#define DROP_CHANCE 75
//Interval between try drop spawn in ms
#define DROP_INTERVAL 250

//Min and max Y for drop end
#define END_Y_MAX 60
#define END_Y_MIN 50

#define SPLASH_SIZE 5

// 128 x 64
SSD1306Wire display(0x3c, D1, D2);

//This stores the next time the system will attempt to create a drop
unsigned long nextDropAttempt;

typedef struct drop {
  int X;
  float Y;
  float EndY;
  float Speed;
  int Splash;
  struct drop* next;
} drop;

drop* rootDrop;

void setup() 
{
  //Serial.begin(115200);
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  
  randomSeed(analogRead(0));
  
  display.init();
  display.flipScreenVertically();

  display.clear();
  display.setFont(ArialMT_Plain_16);
  display.setColor(WHITE);
}

void createDrops()
{
  //Prevent running if it is not time yet
  if(millis() < nextDropAttempt) return;

  //Create drop if chance passes
  if(random(0, 100) < DROP_CHANCE)
  {
    int drop_x = random(0, SCREEN_W);
    float drop_speed = random(DROP_MIN_SPEED, DROP_MAX_SPEED)/100.0;
    int end_y = random(END_Y_MIN, END_Y_MAX);
    createDrop(drop_x, 0, end_y, drop_speed);
  }

  //Schedule next drop
  nextDropAttempt = millis() + DROP_INTERVAL;
}

void createDrop(int X, float Y, float EndY, float Speed)
{
  //Create the drop
  drop* d = (drop*) malloc(sizeof(drop));
  d->X = X;
  d->Y = Y;
  d->Speed = Speed;
  d->EndY = EndY;
  d->Splash = 0;
  d->next = NULL;

  if(d == NULL){
    //Serial.println("malloc failed");
    return;
  }
  
  //Add the drop to the drop list
  if(rootDrop == NULL)
  {
    //Add the first drop
    rootDrop = d;
  }
  else
  {
    //Add as the last drop
    //Get last drop
    drop* lastDrop = rootDrop;
    while(lastDrop != NULL && lastDrop->next != NULL)
    {
      lastDrop = lastDrop->next;
    }
    //Link last drop to my new drop
    if(lastDrop != NULL) 
      lastDrop->next = d;
  }
}

void removeDrop(drop* previous, drop* current)
{
  if(current == NULL) return;
  
  if(previous != NULL)
  {
    //Relink previous to next
    previous->next = current->next;
  }

  if(current == rootDrop && rootDrop != NULL)
  {
    rootDrop = rootDrop->next;
  }
  //Serial.println("Root check "+String(ESP.getFreeHeap()));

  //Really important since this can cause the allocated memory to seem
  //corrupt since it 'appears' it entered the poison boundaries
  current->next = NULL;

  //Delete current
  free(current);
}

void updateDrops()
{
  drop* currentDrop = rootDrop;
  drop* previousDrop = NULL;

  while(currentDrop != NULL)
  {
    //Check drop Y
    if(currentDrop->Y - DROP_LENGTH > currentDrop->EndY)
    {
      if(currentDrop->Splash < SPLASH_SIZE){
        //Update drop splash
        currentDrop->Splash += 1;
      }else{
        //Drop ended, remove it
        removeDrop(previousDrop, currentDrop);
      }
    }else{
      //Move drop Y
      currentDrop->Y += currentDrop->Speed;
    }
    
    //Move to next drop
    previousDrop = currentDrop;
    currentDrop = currentDrop->next;
  }
}

void drawDrops()
{
  drop* currentDrop = rootDrop;
  
  while(currentDrop != NULL)
  {
    //Get drop start Y
    int startY = ((int)currentDrop->Y) - DROP_LENGTH;
    if(startY < 0) startY = 0;

    //Get the drop target Y
    int targetY = (int)currentDrop->Y;
    if(targetY >= currentDrop->EndY) targetY = currentDrop->EndY-1;

    if(currentDrop->Splash > 0){
      //Draw splash
      //display.drawCircle(currentDrop->X, targetY, currentDrop->Splash);
      drawEllipse(&display, currentDrop->X, targetY, currentDrop->Splash+2, currentDrop->Splash);
    }else{
      //Draw drop line
      display.drawLine(currentDrop->X, startY, currentDrop->X, targetY);
    }
    
    //Move to next drop
    currentDrop = currentDrop->next;
  }
}

void loop() 
{
  display.clear();
  
  createDrops();
  updateDrops();
  drawDrops();
  
  display.display();
  
  delay(40);
}
