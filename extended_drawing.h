void plot4EllipsePoints(SSD1306Wire* display, int cx, int cy, int x, int y);

void drawEllipse(SSD1306Wire* display, int cx, int cy, int xRadius, int yRadius)
{
  int x, y;
  int xchange, ychange;
  int ellipseError;
  int twoASquare, twoBSquare;
  int stoppingX, stoppingY;

  twoASquare = 2*xRadius*xRadius;
  twoBSquare = 2*yRadius*yRadius;

  x = xRadius;
  y = 0;

  xchange = yRadius*yRadius*(1-2*xRadius);
  ychange = xRadius*xRadius;
  ellipseError = 0;
  stoppingX = twoBSquare*xRadius;
  stoppingY = 0;

  //1st set of points, y > 1
  
  while(stoppingX >= stoppingY)
  {
    plot4EllipsePoints(display, cx, cy, x, y);
    y++;
    stoppingY += twoASquare;
    ellipseError += ychange;
    ychange += twoASquare;

    if((2*ellipseError + xchange) > 0)
    {
      x--;
      stoppingX -= twoBSquare;
      ellipseError += xchange;
      xchange += twoBSquare;
    }
  }

  //1st point set is done; start the 2nd set of points 

  x = 0;
  y = yRadius;
  xchange = yRadius*yRadius;
  ychange = xRadius*xRadius*(1-2*yRadius);
  ellipseError = 0;
  stoppingX = 0;
  stoppingY = twoASquare*yRadius;

  //2nd set of points, y < 1
  while(stoppingX <= stoppingY)
  {
    plot4EllipsePoints(display, cx, cy, x, y);
    x++;
    stoppingX += twoBSquare;
    ellipseError += xchange;
    xchange += twoBSquare;

    if((2*ellipseError + ychange) > 0)
    {
      y--;
      stoppingY -= twoASquare;
      ellipseError += ychange;
      ychange += twoASquare;
    }
  }
}

void plot4EllipsePoints(SSD1306Wire* display, int cx, int cy, int x, int y)
{
  display->setPixel(cx+x, cy+y);
  display->setPixel(cx-x, cy+y);
  display->setPixel(cx-x, cy-y);
  display->setPixel(cx+x, cy-y);
}
